import React from 'react';

class PokerHand extends React.Component {
    constructor(card) {
        super();
        this.cards = card;
        this.currentHands = '';
        this.currentHand = '';
        this.firstCard = '';
        this.secondCard = '';
    }

    getOutCome = () => {
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                if (i !== j) {
                    if (this.cards[i].rank === this.cards[j].rank ) {
                        this.currentHands = 'One pair!';
                        this.firstCard = this.cards[i].rank;
                        this.secondCard = this.cards[j].rank;

                        for (let k = 0; k < 5; k++) {
                            if (k !== i && k !== j) {
                                if (this.cards[k].rank === this.cards[i].rank) {
                                    this.currentHands = 'Three of a kind!';
                                } else if (this.cards[k].rank !== this.firstCard
                                    && this.cards[k].rank !== this.secondCard) {

                                    for (let l = 0; l < 5; l++) {
                                        if (l !== k) {
                                            if (this.cards[l].rank === this.cards[k].rank) {
                                                this.currentHands = 'Two pairs!';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (this.cards[i].suit === this.cards[j].suit ) {

                        for (let k = 0; k < 5; k++) {
                            if (k !== i && k !== j) {
                                if (this.cards[k].suit === this.cards[i].suit) {

                                    for (let l = 0; l < 5; l++) {
                                        if (l !== i && l !== j && l !== k) {
                                            if (this.cards[l].suit === this.cards[i].suit) {

                                                for (let m = 0; m < 5; m++) {
                                                    if (m !== i && m !== j && m !== k && m !== l) {
                                                        if (this.cards[m].suit === this.cards[i].suit) {
                                                            this.currentHand = 'Flush!'

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

export default PokerHand;