import React from 'react';

class Count extends React.Component {
    constructor(currentHds, currentHd, total) {
        super();

        this.count = null;
        this.totalCount = total;

        if (currentHds === 'One pair!') {
            this.count = 10;
        } else if (currentHds === 'Two pairs!') {
            this.count = 20;
        } else if (currentHds === 'Three of a kind!') {
            this.count = 30;
        } else if (currentHd === 'Flush!') {
            this.count = 50;
        }
        this.totalCount += this.count;
    }
}

export default Count;