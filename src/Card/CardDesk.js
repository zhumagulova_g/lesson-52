import React from 'react';

class CardDesk  extends React.Component {
    constructor () {
        super();

        this.suits = {
            diams: "diams",
            hearts: "hearts",
            spades: "spades",
            clubs: "clubs",
        };

        this.rank = {
            two: "2",
            three: "3",
            four: "4",
            five: "5",
            six: "6",
            seven: "7",
            eight: "8",
            nine: "9",
            ten: "10",
            jack: "j",
            queen: "q",
            king: "k",
            ace: "a"
        };

        this.cards = [];
        this.colode = [];

        for (let key in this.suits) {
            if (!this.suits.hasOwnProperty(key)) {
                continue;
            }
            for (let inKey in this.rank) {
                if (!this.rank.hasOwnProperty(inKey)) {
                    continue;
                }
                this.cards.push({suit: this.suits[key], rank: this.rank[inKey]});
            }
        }
    }
    getCard = () => {
        const random = Math.floor(Math.random()*this.cards.length);

        this.colode.push(this.cards[random]);
        this.cards.splice(random,1);

    };

    getCards = (howMany) => {
        for (let i = 0; i < howMany; i++) {
            this.getCard();
        }
    };
}

export default CardDesk;
