import React from 'react';


const Card = props => {
    let suit;

    if (props.suit === "diams") {
        suit = "♦";
    } else if(props.suit === "hearts") {
        suit = "♥";
    } else if (props.suit === "spades") {
        suit = "♠";
    } else  if (props.suit === "clubs") {
        suit = "♣";
    }

        return (

        <div className={"card rank-" + props.rank + " " + props.suit}>
            <span className="rank">{props.rank}</span>
            <span className="suit">{suit}</span>
        </div>
    );
};

export default Card;