import React from 'react';
import './App.css';
import './Card/Card.css'
import Card from './Card/Card';
import PokerHand from './Card/PokerHand';
import CardDesk from './Card/CardDesk';
import Count from './Card/Count';

class App extends React.Component {
    state = {
      card: [{suit: '', rank: ''}, {suit: '', rank: ''}, {suit: '', rank: ''}, {suit: '', rank: ''}, {suit: '', rank: ''}],
        currentHds: '',
        currentHd: '',
        total: 0,
    };

    createNewDeck = () => {
        const cardDesk = new CardDesk();
        cardDesk.getCards(5);

        const pokerHand = new PokerHand(cardDesk.colode);
        pokerHand.getOutCome();

        const counter = new Count(pokerHand.currentHands, pokerHand.currentHand, this.state.total);

        this.setState({
            total: counter.totalCount,
            card: cardDesk.colode,
            currentHds: pokerHand.currentHands,
            currentHd: pokerHand.currentHand,
        });
    };

    render() {
        return (
            <div className="App">
                <div className="playingCards">
                    <Card suit={this.state.card[0].suit} rank={this.state.card[0].rank} />
                    <Card suit={this.state.card[1].suit} rank={this.state.card[1].rank} />
                    <Card suit={this.state.card[2].suit} rank={this.state.card[2].rank} />
                    <Card suit={this.state.card[3].suit} rank={this.state.card[3].rank} />
                    <Card suit={this.state.card[4].suit} rank={this.state.card[4].rank} />
                    <div className="current-hands">{'Current hands: ' + this.state.currentHds + '  ' + this.state.currentHd}</div>
                    <div>{'One pair: 10 points;'}</div>
                    <div>{'Two pairs: 20 points;'}</div>
                    <div>{'Three of a kind: 30 points;'}</div>
                    <div>{'Flush: 50 points'}</div>
                    <div className="current-hands">{'Your win: ' + this.state.total + ' points.'}</div>
                    <div>
                        <button onClick={this.createNewDeck}>New deck</button>
                    </div>

                </div>

            </div>
        );
    }
}

export default App;
